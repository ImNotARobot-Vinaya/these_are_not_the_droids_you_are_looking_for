As far as foul-mouthed alcoholic robots go, Bender is pretty hard to beat. For seven seasons on Futurama he told us to bite his shiny metal ass, and promised us theme parks with blackjack and hookers. In an effort to keep what should be his immortal words alive, here are twenty Bender quotes that every Futurama fan should know by heart.

1. “This is the worst kind of discrimination there is: the kind against me!”

From “War Is The H-Word,” when he is unable to buy gum with a military discount.

2. “I guess if you want children beaten, you have to do it yourself.”

From “The Route Of All Evil,” disappointed that Hermes and the professor won’t take harsher action against their sons.

3. “Hahahahaha. Oh wait you’re serious. Let me laugh even harder.”

From “Love And Rocket,” when Leela asks him if he has any respect for his girlfriend’s feelings.

4. “There. Now no one can say I don’t own John Larroquette’s spine.”

From “The Luck Of The Fry-rishh,” on a fruitful grave-robbing trip.

5. “I’ll build by own theme park. With black jack, and hookers. In fact, forget the park!”

From “The Series Has Landed,” after being kicked out of Luna Park

6. “The game’s over. I have all the money. Compare your lives to mine and then kill yourselves!”

From “A Head In The Polls,” enjoying the money he made from selling his body.

7. “That’s closest thing to ‘Bender is great’ that anyone other me has ever said.”

From “Ghost In The Machines,” hearing Fry’s touching epitaph for him.

8. “I’m Bender, baby! Oh god, please insert liquor!”

From “How Hermes Requisitioned His Grovveback,” after finally having his artificial intelligence turned back on.

9. “Hey sexy mama, wanna kill all humans?”

From “I, Roommate,” having a dream that frightens Fry just a little bit.

10. “You know what cheers me up? Other people’s misfortune.”

From “The Devil’s Hands Are Idle Plaything,” basking in Leela’s misfortune.

11. “Anything less than immortality is a complete waste of time.”

From “Lethal Inspection,” finding out he’s not immortal.

12. “Blackmail is such an ugly word. I prefer extortion. The ‘x’ makes it sound cool.”

From “Anthology Of Interest I,” extorting Leela after she kills the professor.

13. “Have you tried turning off the TV, sitting down with your children, and hitting them?”

From “Bender Should Not Be Allowed On Television,” offering advice to fathers everywhere.

14. “Fry cracked corn and I don’t care/Leela cracked corn I still don’t care/Bender cracked corn and he is great/ Take that you stupid corn!”

From “Bendin’ In The Wind,” indulging his dream of being a folk singer one last time.

15. “Oh, your God!”

From “Amazon Women In The Mood,” reacting to seeing Planet Amazonia for the first time.

16. “You’re a pimple on society’s ass and you’ll never amount to anything!”

From “My Three Suns,” trying to make Fry cry.

17. “Shut up baby, I know it!”

From “Raging Bender,” when one was his floozies says “I love you.”

18. “I’m so embarrassed. I wish everyone else was dead!”

im-so-embarassed-i-wish-everybody-else-was-dead
FOX

From “Bend Her,” not able to win any bending events at the Olympics. At least not whet competing against men.

19. “I got ants my butt, and I needs to strut!”

From “Bender Should Not Be Allowed On Television,” immediately creating a catchphrase on All My Circuits.

20. “Afterlife? If I thought I had to live another life, I’d kill myself right now!”

From ” A Pharoah To Remember,” after being prematurely shoved into his sarcophagus.
